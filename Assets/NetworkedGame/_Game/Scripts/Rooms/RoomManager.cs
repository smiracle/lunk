﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoomData
{
	public string Name;
	public int X;
	public int Y;
}

public class RoomManager : Photon.MonoBehaviour
{
	public static RoomManager Instance;

	string m_CurrentWorldName = "Overworld";

	RoomData m_CurrentLoadRoomData;
	Queue<RoomData> m_LoadRoomQueue = new Queue<RoomData>();
	string[] namesOfRoomsToLoadArray = new string[4];
	int[] roomsX = new int[4];
	int[] roomsY = new int[4];
	List<RoomParent> m_LoadedRooms = new List<RoomParent>();

	bool m_IsLoadingRoom = false;

	void Awake()
	{
		Instance = this;
	}

	void Start()
	{

		LoadRoom("Start", 0, 0);
		LoadRoom("End", 1, 0);

		LoadRoom("Empty", 1, 0);
		LoadRoom("Empty", 0, -1);
		LoadRoom("Empty", -1, 0);
	}

	void Update()
	{
		UpdateRoomQueue();
	}

	void UpdateRoomQueue()
	{
		if (m_IsLoadingRoom == true)
		{
			return;
		}

		if (m_LoadRoomQueue.Count == 0)
		{
			return;
		}

		m_CurrentLoadRoomData = m_LoadRoomQueue.Dequeue();
		m_IsLoadingRoom = true;

		//Debug.Log( "Loading new room: " + m_CurrentLoadRoomData.Name + " at " + m_CurrentLoadRoomData.X + ", " + m_CurrentLoadRoomData.Y );

		StartCoroutine(LoadRoomRoutine(m_CurrentLoadRoomData));
	}

	void LoadRoom(string name, int x, int y)
	{
		if (DoesRoomExist(x, y) == true)
		{
			return;
		}

		RoomData newRoomData = new RoomData();
		newRoomData.Name = name;
		newRoomData.X = x;
		newRoomData.Y = y;

		m_LoadRoomQueue.Enqueue(newRoomData);
	}

	IEnumerator LoadRoomRoutine(RoomData data)
	{
		string levelName = m_CurrentWorldName + data.Name;

		AsyncOperation loadLevel = Application.LoadLevelAdditiveAsync(levelName);

		while (loadLevel.isDone == false)
		{
			//Debug.Log("Loading " + levelName + ": " + Mathf.Round(loadLevel.progress * 100) + "%");
			yield return null;
		}
	}

	public void RegisterRoom(RoomParent roomParent)
	{
		roomParent.transform.position = new Vector3(
		    m_CurrentLoadRoomData.X * roomParent.Width,
		    m_CurrentLoadRoomData.Y * roomParent.Height, 0);

		roomParent.X = m_CurrentLoadRoomData.X;
		roomParent.Y = m_CurrentLoadRoomData.Y;
		roomParent.name = m_CurrentWorldName + "-" + m_CurrentLoadRoomData.Name + " " + roomParent.X + ", " + roomParent.Y;
		roomParent.transform.parent = transform;

		m_IsLoadingRoom = false;
		m_LoadedRooms.Add(roomParent);
	}

	bool DoesRoomExist(int x, int y)
	{
		return m_LoadedRooms.Find(item => item.X == x && item.Y == y) != null;
	}

	string GetRandomRegularRoomName()
	{
		string[] possibleRooms = new string[] {
		"Empty",
		"Regular00",
		"Regular01",
		"Regular02",
		"Regular03",
		"Regular04",
		"Puzzle01",
	  };

		return possibleRooms[Random.Range(0, possibleRooms.Length)];
	}

	private void SpawnEnemies(RoomParent roomParent)
	{
		roomParent.enemiesSpawned = true;
		Vector3 roomCenter = GameCamera.Instance.CurrentRoom.GetRoomCenter();
		BatSpawner[] batSpawners = Object.FindObjectsOfType<BatSpawner>();
		foreach (BatSpawner batSpawn in batSpawners)
		{
			if (batSpawn.gameObject.transform.IsChildOf(GameCamera.Instance.CurrentRoom.transform))
			{
				//Vector3 loc = new Vector3(roomParent.X * roomParent.Width + bat.transform.position.x, roomParent.Y * roomParent.Height + bat.transform.position.y);
				Vector3 loc = new Vector3(batSpawn.transform.position.x, batSpawn.transform.position.y);
				PhotonNetwork.Instantiate("Bat", loc, Quaternion.identity, 0);
				Destroy(batSpawn.gameObject);
			}
		}
	}

	public void OnPlayerEnterRoom(RoomParent roomParent)
	{
		GameCamera.Instance.CurrentRoom = roomParent;

		if (!roomParent.enemiesSpawned)
		{
			SpawnEnemies(roomParent);
		}
		else
		{
			Debug.Log("Already spawned enemies for the room.");
		}

		for (int i = 0; i < namesOfRoomsToLoadArray.Length; i++)
		{
			namesOfRoomsToLoadArray[i] = GetRandomRegularRoomName();
		}

		roomsX[0] = roomParent.X + 1;
		roomsX[1] = roomParent.X - 1;
		roomsX[2] = roomParent.X;
		roomsX[3] = roomParent.X;

		roomsY[0] = roomParent.Y;
		roomsY[1] = roomParent.Y;
		roomsY[2] = roomParent.Y + 1;
		roomsY[3] = roomParent.Y - 1;

		if (roomParent.X == 0 && roomParent.Y == 0)
		{
			return; //Already loaded adjacent rooms on Start();
		}

		if (PhotonNetwork.playerList.Length == 1)
		{
			LoadNewRooms();
		}
		else
		{
			PhotonView photonView = PhotonView.Get(this);
			photonView.RPC("SendRoomGenerationData", PhotonTargets.AllBuffered, namesOfRoomsToLoadArray, roomsX, roomsY);
		}
	}	

	[PunRPC]
	void SendRoomGenerationData(string[] namesOfRoomsToLoadArr, int[] rmsX, int[] rmsY) //PhotonMessageInfo info
	{
		namesOfRoomsToLoadArray = namesOfRoomsToLoadArr;
		roomsX = rmsX;
		roomsY = rmsY;

		//Debug.Log(String.Format("Info: {0} {1} {2}", info.sender, info.photonView, info.timestamp));
		LoadNewRooms();
		//Assets.NetworkedGame._Game.Scripts.MyPUNScripts.DebugConsole.Log("SendRoomGenerationReceived");
	}

	public void LoadNewRooms()
	{
		//Debug.Log(rootRoom.X + ", " + rootRoom.Y);
		LoadRoom(namesOfRoomsToLoadArray[0], roomsX[0], roomsY[0]);
		LoadRoom(namesOfRoomsToLoadArray[1], roomsX[1], roomsY[1]);
		LoadRoom(namesOfRoomsToLoadArray[2], roomsX[2], roomsY[2]);
		LoadRoom(namesOfRoomsToLoadArray[3], roomsX[3], roomsY[3]);
	}
}