﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIHealthBar : MonoBehaviour
{
	private CharacterHealthModel HealthModel;
	public Text HealthText;
	public RectTransform HealthBar;

	public void SetHealthModel(CharacterHealthModel chm)
	{
		HealthModel = chm;
	}

	void Update()
	{
		UpdateText();
		UpdateHealthBar();
	}

	void UpdateText()
	{
		HealthText.text = Mathf.RoundToInt(HealthModel.GetHealth()) + "/" +
		Mathf.RoundToInt(HealthModel.GetMaximumHealth());
	}

	void UpdateHealthBar()
	{
		HealthBar.localScale = new Vector3(HealthModel.GetHealthPercentage(), 1f, 1f);
	}
}
