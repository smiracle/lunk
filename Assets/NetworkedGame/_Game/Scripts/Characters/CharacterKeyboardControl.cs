﻿using UnityEngine;
using System.Collections;

public class CharacterKeyboardControl : CharacterBaseControl
{
	private bool isLocalInstance;
	void Start()
	{
		PhotonView pv = PhotonView.Get(this);

		if (pv.isMine)
		{
			// initialize whatever you need to for the LOCAL player
			isLocalInstance = true;
			
		}
		else
		{
			// destroy or disable things for REMOTE players
			isLocalInstance = false;			
			GetComponent<CharacterInteractionModel>().enabled = false;			
			GetComponent<CharacterMovementModel>().enabled = false;
			GetComponent<CharacterMovementAdvancedView>().enabled = false;
			GetComponent<CharacterKeyboardControl>().enabled = false;
			GetComponent<CharacterMovementView>().enabled = false;
		}

		SetDirection(new Vector2(0, -1));
	}

	void Update()
	{
		if (isLocalInstance)
		{
			UpdateDirection();
			UpdateAction();
			UpdateAttack();
		}
	}

	void UpdateAttack()
	{
		if (Input.GetKeyDown(KeyCode.Mouse0)|| Input.GetKeyDown(KeyCode.R))
		{
			OnAttackPressed();
		}
	}

	void UpdateAction()
	{
		if (Input.GetKeyDown(KeyCode.E))
		{
			OnActionPressed();
		}
	}

	void UpdateDirection()
	{
		Vector2 newDirection = Vector2.zero;

		if (Input.GetKey(KeyCode.W))
		{
			newDirection.y = 1;
		}

		if (Input.GetKey(KeyCode.S))
		{
			newDirection.y = -1;
		}

		if (Input.GetKey(KeyCode.A))
		{
			newDirection.x = -1;
		}

		if (Input.GetKey(KeyCode.D))
		{
			newDirection.x = 1;
		}

		SetDirection(newDirection);
	}
}
