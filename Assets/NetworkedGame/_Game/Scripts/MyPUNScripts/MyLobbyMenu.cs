using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class MyLobbyMenu : MonoBehaviour
{
	private string roomName = "myRoom";
	private bool connectFailed = false;
	public static readonly string SceneNameMenu = "Lobby-Scene";
	public static readonly string SceneNameGame = "Game-Scene";
	private string errorDialog;
	private double timeToClearDialog;

	//UI Controls:
	public Text txtServerMessage;
	public Text txtAvailableRooms;
	public Text txtTitle;
	public Text txtError;
	public GameObject infoPanel;
	public GameObject btnTryAgain;
	public Button btnCreateRoom;
	public GameObject panelAvailableRoomPrefab;
	public GameObject panelAvailbleRoomScrollPanel;
	public GameObject panelScrollView;
	public InputField inputPlayerName;
	public InputField inputRoomName;

	private int availableRoomCount;
	private int previousAvailableRoomCount;

	public string ErrorDialog
	{
		get { return this.errorDialog; }
		private set
		{
			this.errorDialog = value;
			if (!string.IsNullOrEmpty(value))
			{
				this.timeToClearDialog = Time.time + 4.0f;
			}
		}
	}

	public void Awake()
	{
		// this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
		PhotonNetwork.automaticallySyncScene = true;

		// the following line checks if this client was just created (and not yet online). if so, we connect
		if (PhotonNetwork.connectionStateDetailed == PeerState.PeerCreated)
		{
			// Connect to the photon master-server. We use the settings saved in PhotonServerSettings (a .asset file in this project)
			PhotonNetwork.ConnectUsingSettings("0.9");
		}

		// generate a name for this player, if none is assigned yet
		if (String.IsNullOrEmpty(PhotonNetwork.playerName))
		{
			PhotonNetwork.playerName = "Guest" + Random.Range(1, 9999);
		}

		// if you wanted more debug out, turn this on:
		// PhotonNetwork.logLevel = NetworkLogLevel.Full;
	}

	private void Start()
	{
		infoPanel.SetActive(false);
		panelScrollView.SetActive(false);
		btnTryAgain.SetActive(false);
		previousAvailableRoomCount = -1;
		inputPlayerName.text = PhotonNetwork.playerName;		
		inputRoomName.text = this.roomName;
	}

	public void Update()
	{
		if (!PhotonNetwork.connected)
		{
			if (PhotonNetwork.connecting)
			{
				txtServerMessage.text = "Connecting to: " + PhotonNetwork.ServerAddress;
			}
			else
			{
				txtServerMessage.text = "Not connected. Check console output. Detailed connection state: " + PhotonNetwork.connectionStateDetailed + " Server: " + PhotonNetwork.ServerAddress;
			}

			if (this.connectFailed)
			{
				txtServerMessage.text = "Connection failed. Check setup and use Setup Wizard to fix configuration.";
				txtServerMessage.text += "\n" + String.Format("Server: {0}", new object[] { PhotonNetwork.ServerAddress });
				txtServerMessage.text += "\n" + "AppId: " + PhotonNetwork.PhotonServerSettings.AppID.Substring(0, 8) + "****"; // only show/log first 8 characters. never log the full AppId.
				btnTryAgain.SetActive(true);
			}

			return;
		}
		if (!infoPanel.GetActive())
		{
			txtServerMessage.GetComponent<Text>().enabled = false;
			infoPanel.SetActive(true);
			panelScrollView.SetActive(true);
		}

		if (!string.IsNullOrEmpty(ErrorDialog))
		{
			//    GUILayout.Label(ErrorDialog);
			txtError.text = ErrorDialog;
			if (this.timeToClearDialog < Time.time)
			{
				this.timeToClearDialog = 0;
				ErrorDialog = "";
			}
		}
		PopulateRoomList();
	}

	void PopulateRoomList()
	{
		RoomInfo[] roomArr = PhotonNetwork.GetRoomList();
		if (previousAvailableRoomCount != roomArr.Length)
		{
			previousAvailableRoomCount = roomArr.Length;
			//Debug.Log("populateroomlist");

			for (int i = 0; i < roomArr.Length; i++)
			{
				Debug.Log(i);
				Debug.Log("Room '" + roomArr[i].name + "' found in list");
				//RoomInfo roomInfo in 
				GameObject newItem = Instantiate(panelAvailableRoomPrefab) as GameObject;
				newItem.transform.SetParent(panelAvailbleRoomScrollPanel.transform, false);
				newItem.GetComponent<RoomMenuItem>().SetRoomName(roomArr[i].name);
				newItem.SetActive(true);
				newItem.name = newItem.name + roomArr[i].name;
				newItem.GetComponentInChildren<Text>().text = roomArr[i].name;
			}
		}
	}

	//PhotonNetwork.CreateRoom("Room #" + 1, new RoomOptions() { maxPlayers = 2 }, null);
	//    PhotonNetwork.CreateRoom("Room #" + 2, new RoomOptions() { maxPlayers = 2 }, null);
	//    PhotonNetwork.CreateRoom("Room #" + 3, new RoomOptions() { maxPlayers = 2 }, null);

	private bool creatingRoom;
	public void OnClickCreateRoom()
	{				
		if (!string.IsNullOrEmpty(inputPlayerName.text) && !string.IsNullOrEmpty(inputRoomName.text) && !creatingRoom)
		{
			creatingRoom = true;
			this.roomName = inputRoomName.text;
			PhotonNetwork.CreateRoom(this.roomName, new RoomOptions() { maxPlayers = 4 }, null);
		}		
	}
	public void OnClickJoinRoom(string roomName)
	{
		Debug.Log("clicked join room button, joining room '" + roomName + "'");
		PhotonNetwork.JoinRoom(roomName);
	}
	public void OnNameTextChange()
	{
		PhotonNetwork.playerName = inputPlayerName.text;
		PlayerPrefs.SetString("playerName", PhotonNetwork.playerName);
	}
	public void OnClickTryAgain()
	{
		this.connectFailed = false;
		PhotonNetwork.ConnectUsingSettings("0.9");
		btnTryAgain.SetActive(false);
	}

	// We have two options here: we either joined(by title, list or random) or created a room.
	public void OnJoinedRoom()
	{		
		Debug.Log("OnJoinedRoom");
	}
	public void OnPhotonCreateRoomFailed()
	{
		ErrorDialog = "Error: Can't create room (room name maybe already used).";
		Debug.Log("OnPhotonCreateRoomFailed got called. This can happen if the room exists (even if not visible). Try another room name.");
	}
	public void OnPhotonJoinRoomFailed(object[] cause)
	{
		ErrorDialog = "Error: Can't join room (full or unknown room name). " + cause[1];
		Debug.Log("OnPhotonJoinRoomFailed got called. This can happen if the room is not existing or full or closed.");
	}
	public void OnPhotonRandomJoinFailed()
	{
		ErrorDialog = "Error: Can't join random room (none found).";
		Debug.Log("OnPhotonRandomJoinFailed got called. Happens if no room is available (or all full or invisible or closed). JoinrRandom filter-options can limit available rooms.");
	}
	public void OnCreatedRoom()
	{
		//Debug.Log("OnCreatedRoom");
		PhotonNetwork.LoadLevel(SceneNameGame);
		creatingRoom = false;
	}
	public void OnDisconnectedFromPhoton()
	{
		Debug.Log("Disconnected from Photon.");
	}
	public void OnFailedToConnectToPhoton(object parameters)
	{
		this.connectFailed = true;
		Debug.Log("OnFailedToConnectToPhoton. StatusCode: " + parameters + " ServerAddress: " + PhotonNetwork.ServerAddress);
	}
	public void OnReceivedRoomList()
	{
		Debug.Log("OnReceivedRoomList");
	}
	public void OnReceivedRoomListUpdate()
	{
		//Debug.Log("OnReceivedRoomListUpdate");
	}
}
