﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(PhotonView))]
public class MyInRoomChat : Photon.MonoBehaviour
{
    //public Rect GuiRect = new Rect(0, 0, 250, 300);
    public bool IsVisible = true;
    //public bool AlignBottom = false;
    public List<string> messages = new List<string>();
    private string inputLine = "";
    private int previousMessageCount;

    //UI Controls    
    public Button btnSend;
    public InputField inputChat;
    public Text txtChat;
    public Scrollbar scrollbar;
    public ScrollRect scrollRect;

    public static readonly string ChatRPC = "Chat";

    public void Start()
    {
        this.previousMessageCount = messages.Count;
        for (int i = messages.Count - 1; i >= 0; i--)
        {
            txtChat.text += "\n" + messages[i];
        }
        scrollRect.verticalNormalizedPosition = 0;
        //scrollbar.GetComponent<Scrollbar>().value = 0f;
    }


    void Update()
    {
        if (!this.IsVisible || PhotonNetwork.connectionStateDetailed != PeerState.Joined)
        {
            return;
        }
        inputChat.Select();
        inputChat.ActivateInputField();
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            //Event.current.type == EventType.KeyDown && (Event.current.keyCode == KeyCode.KeypadEnter || Event.current.keyCode == KeyCode.Return)
 
            if (!string.IsNullOrEmpty(this.inputLine))
            {
                this.photonView.RPC("Chat", PhotonTargets.All, this.inputLine);
                this.inputLine = "";
                //GUI.FocusControl("");                
                return; // printing the now modified list would result in an error. to avoid this, we just skip this single frame
            }
        }
        if (this.previousMessageCount != messages.Count)
        {
            this.previousMessageCount = messages.Count;
            inputChat.Select();
            inputChat.text = "";
            if (messages.Count > 0)
            {
                txtChat.text += messages[messages.Count - 1] + "\n";
            }
            scrollRect.verticalNormalizedPosition = 0;                        
            //scrollbar.GetComponent<Scrollbar>().value = 0f;
        }
        this.inputLine = inputChat.text;
    }

    public void OnClickSendButton()
    {
        if (!string.IsNullOrEmpty(this.inputLine))
        {
            this.photonView.RPC("Chat", PhotonTargets.All, this.inputLine);
            this.inputLine = "";
        }
    }

    [PunRPC]
    public void Chat(string newLine, PhotonMessageInfo mi)
    {
        string senderName = "anonymous";

        if (mi != null && mi.sender != null)
        {
            if (!string.IsNullOrEmpty(mi.sender.name))
            {
                senderName = mi.sender.name;
            }
            else
            {
                senderName = "player " + mi.sender.ID;
            }
        }
        this.messages.Add(senderName + ": " + newLine);
    }

    public void AddLine(string newLine)
    {
        this.messages.Add(newLine);
    }
}