﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RoomMenuItem : MonoBehaviour {

    private string roomName;
    private Button joinRoomButton;
    private MyLobbyMenu menu;
    void Start()
    {
        this.menu = GameObject.FindObjectOfType<MyLobbyMenu>();
    }
    public void SetRoomName(string roomName)
    {
        this.roomName = roomName;
    }
    public void OnClickRoomItemButton()
    {        
        menu.OnClickJoinRoom(this.roomName);
    }
}
